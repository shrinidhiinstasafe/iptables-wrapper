package entities;

import java.util.LinkedList;

public class MangleEntity {

    private LinkedList<String> prerouting;
    private LinkedList<String> postrouting;
    private LinkedList<String> input;
    private LinkedList<String> output;
    private LinkedList<String> forward;

    public LinkedList<String> getPrerouting() {
        return this.prerouting;
    }

    public void setPrerouting(LinkedList<String> prerouting) {
        this.prerouting = prerouting;
    }

    public LinkedList<String> getPostrouting() {
        return this.postrouting;
    }

    public void setPostrouting(LinkedList<String> postrouting) {
        this.postrouting = postrouting;
    }

    public LinkedList<String> getInput() {
        return this.input;
    }

    public void setInput(LinkedList<String> input) {
        this.input = input;
    }

    public LinkedList<String> getOutput() {
        return this.output;
    }

    public void setOutput(LinkedList<String> output) {
        this.output = output;
    }

    public LinkedList<String> getForward() {
        return this.forward;
    }

    public void setForward(LinkedList<String> forward) {
        this.forward = forward;
    }

}