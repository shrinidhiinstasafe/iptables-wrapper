package entities;

import constants.Parameters;

/**
 * This is a builder class which (in conjugation to an Entity Class) will build
 * a rule by setting the appropriate parameters. After building, we can retrieve
 * the rule by using toString()
 * 
 * @author Shrinidhi Rao shrinidhi@instasafe.com
 */

public class Rule {
    /**  This is the program name. The default and currently supported is <code>iptables</code> */
    private String program = "iptables "; // First in the hierarchy.
    private String operation; // Second. See Operation file in constants.
    private String option; // 
    private String subOptions;
    private String target;
    private String miscellaneous;
    private String log;

    private Rule(RuleBuilder builder) {
        this.operation = builder.getOperation();
        this.target = builder.getTarget();
        this.option = builder.getOption();
        this.subOptions = builder.getSubOptions();
        this.miscellaneous = builder.getMiscellaneous();
        this.log = builder.getLog();
    }

    @Override
    public String toString() {

        String construct = program;
        if (operation != null)
            construct += operation;
        if (option != null)
            construct += option;
        if (subOptions != null)
            construct += subOptions;
        if (miscellaneous != null)
            construct += miscellaneous;
        if (target != null)
            construct += Parameters.jump + target;
        if (log != null)
            construct += log;
        

        return construct;
    }

    public static class RuleBuilder {
        private String operation;
        private String target;
        private String option;
        private String subOptions;
        private String miscellaneous;
        private String log;

        public String getLog(){
            return this.log;
        }
        public String getOperation() {
            return this.operation;
        }

        public String getTarget() {
            return this.target;
        }

        public String getOption() {
            return this.option;
        }

        public String getSubOptions() {
            return this.subOptions;
        }

        public String getMiscellaneous() {
            return this.miscellaneous;
        }
        
        public RuleBuilder log(String log){
            this.log = log;
            return this;
        }

        public RuleBuilder operation(String operation) {
            this.operation = operation;
            return this;
        }

        public RuleBuilder target(String target) {
            this.target = target;
            return this;
        }

        public RuleBuilder option(String option) {
            this.option = option;
            return this;
        }

        public RuleBuilder subOptions(String subOptions) {
            this.subOptions = subOptions;
            return this;
        }

        public RuleBuilder miscellaneous(String miscellaneous) {
            this.miscellaneous = miscellaneous;
            return this;
        }

        public Rule build() {
            return new Rule(this);
        }

    }
}