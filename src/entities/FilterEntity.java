package entities;

import java.util.LinkedList;

public class FilterEntity{
    private LinkedList<String> input;
    private LinkedList<String> output;
    private LinkedList<String> forward;


    public FilterEntity() {
    }

    public FilterEntity(LinkedList<String> input, LinkedList<String> output, LinkedList<String> forward) {
        this.input = input;
        this.output = output;
        this.forward = forward;
    }

    public LinkedList<String> getInput() {
        return this.input;
    }

    public void setInput(LinkedList<String> input) {
        this.input = input;
    }

    public LinkedList<String> getOutput() {
        return this.output;
    }

    public void setOutput(LinkedList<String> output) {
        this.output = output;
    }

    public LinkedList<String> getForward() {
        return this.forward;
    }

    public void setForward(LinkedList<String> forward) {
        this.forward = forward;
    }

}