package entities;

import java.util.LinkedList;

public class NatEntity {

    private LinkedList<String> prerouting;
    private LinkedList<String> postrouting;
    private LinkedList<String> output;

    public LinkedList<String> getPrerouting() {
        return this.prerouting;
    }

    public void setPrerouting(LinkedList<String> prerouting) {
        this.prerouting = prerouting;
    }

    public LinkedList<String> getPostrouting() {
        return this.postrouting;
    }

    public void setPostrouting(LinkedList<String> postrouting) {
        this.postrouting = postrouting;
    }

    public LinkedList<String> getOutput() {
        return this.output;
    }

    public void setOutput(LinkedList<String> output) {
        this.output = output;
    }

}