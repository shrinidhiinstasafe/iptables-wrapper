package entities;

import java.util.LinkedList;

public class RawEntity {

    private LinkedList<String> prerouting;
    private LinkedList<String> output;


    public RawEntity() {
    }

    public RawEntity(LinkedList<String> prerouting, LinkedList<String> output) {
        this.prerouting = prerouting;
        this.output = output;
    }

    public LinkedList<String> getPrerouting() {
        return this.prerouting;
    }

    public void setPrerouting(LinkedList<String> prerouting) {
        this.prerouting = prerouting;
    }

    public LinkedList<String> getOutput() {
        return this.output;
    }

    public void setOutput(LinkedList<String> output) {
        this.output = output;
    }

}