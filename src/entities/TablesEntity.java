package entities;

public class TablesEntity {

    private FilterEntity filterTable;
    private NatEntity natTable;
    private MangleEntity mangleTable;
    private RawEntity rawTable;

    public FilterEntity getFilterTable() {
        return this.filterTable;
    }

    public void setFilterTable(FilterEntity filterTable) {
        this.filterTable = filterTable;
    }

    public NatEntity getNatTable() {
        return this.natTable;
    }

    public void setNatTable(NatEntity natTable) {
        this.natTable = natTable;
    }

    public MangleEntity getMangleTable() {
        return this.mangleTable;
    }

    public void setMangleTable(MangleEntity mangleTable) {
        this.mangleTable = mangleTable;
    }

    public RawEntity getRawTable() {
        return this.rawTable;
    }

    public void setRawTable(RawEntity rawTable) {
        this.rawTable = rawTable;
    }

}