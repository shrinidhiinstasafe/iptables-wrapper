package constants;

public enum Table {

    ;

    public enum Nat {
        ;

        public enum Chain {
            prerouting(" PREROUTING "), postrouting(" POSTROUTING "), output(" OUTPUT ");

            private String chainString;

            Chain(String chainString) {
                this.chainString = chainString;
            }

            public String getChain() {
                return this.chainString;
            }

            public String getChainSaveFile(){
                return ":"+this.chainString.trim();
            }

        }

        public static String getTableRule() {
            return " NAT ";
        }

        public static String getSaveString() {
            return "*nat";
        }
    };

    public enum Raw {
        ;

        public enum Chain {
            prerouting(" PREROUTING "), output(" OUTPUT ");

            private String chainString;

            Chain(String chainString) {
                this.chainString = chainString;
            }

            public String getChain() {
                return this.chainString;
            }

            public String getChainSaveFile(){
                return ":"+this.chainString.trim();
            }

        }

        public static String getTableRule() {
            return " RAW ";
        }

        public static String getSaveString() {
            return "*raw";
        }

    };

    public enum Filter {
        ;

        public enum Chain {
            input(" INPUT "), output(" OUTPUT "), forward(" FORWARD ");

            String local;

            Chain(String local) {
                this.local = local;
            }

            public String getChain() {
                return local;
            }

            public String getChainSaveFile(){
                return ":"+local.trim();
            }

        };
        /**
         * 
         * @return FILTER
         */
        public static String getTableRule() {
            return " FILTER ";
        }
        /**
         * 
         * @return *filter
         */
        public static String getSaveString() {
            return "*filter";
        }

    };

    public enum Mangle {
        ;

        public enum Chain {
            prerouting(" PREROUTING "), postrouting(" POSTROUTING "), input(" INPUT "), output(" OUTPUT "),
            forward(" FORWARD ");

            String local;

            Chain(String local) {
                this.local = local;
            }

            public String getChain() {
                return local;
            }

            public String getChainSaveFile(){
                return ":"+local.trim();
            }


        };

        public static String getTableRule() {
            return " MANGLE ";
        }

        public static String getSaveString() {
            return "*mangle";
        }

    };
}
