package constants;

public class Parameters {

    /**
     * The protocol of the rule or of the packet to check. The specified protocol
     * can be one of tcp, udp, icmp, or all, or it can be a numeric value,
     * representing one of these protocols or a different one. A protocol name from
     * /etc/protocols is also allowed. A "!" argument before the protocol inverts
     * the test. The number zero is equivalent to all. Protocol all will match with
     * all protocols and is taken as default when this option is omitted. @apiNote
     * --protocol [!] protocol
     */

    public static final String protocol = " -p "; // Constants to be created.

    /**
     * Source specification. Address can be either a network name, a hostname
     * (please note that specifying any name to be resolved with a remote query such
     * as DNS is a really bad idea), a network IP address (with /mask), or a plain
     * IP address. The mask can be either a network mask or a plain number,
     * specifying the number of 1's at the left side of the network mask. Thus, a
     * mask of 24 is equivalent to 255.255.255.0. A "!" argument before the address
     * specification inverts the sense of the address. The flag -/-src is an alias
     * for this option. @apiNote --source [!] address[/mask]
     */

    public static final String source = " -s ";

    /**
     * Destination specification. See the description of the -s (source) flag for a
     * detailed description of the syntax. The flag -\-dst is an alias for this
     * option. @apiNote --destination [!] address[/mask]
     */

    public static final String destination = " -d ";

    /**
     * This specifies the target of the rule; i.e., what to do if the packet matches
     * it. The target can be a user-defined chain (other than the one this rule is
     * in), one of the special builtin targets which decide the fate of the packet
     * immediately, or an extension (see EXTENSIONS below). If this option is
     * omitted in a rule (and -g is not used), then matching the rule will have no
     * effect on the packet's fate, but the counters on the rule will be
     * incremented. @apiNote --jump target
     */

    public static final String jump = " -j ";

    /**
     * This specifies that the processing should continue in a user specified chain.
     * Unlike the --jump option return will not continue processing in this chain
     * but instead in the chain that called us via --jump. @apiNote --goto chain
     */

    public static final String gotoChain = " -g ";

    /**
     * Name of an interface via which a packet was received (only for packets
     * entering the INPUT, FORWARD and PREROUTING chains). When the "!" argument is
     * used before the interface name, the sense is inverted. If the interface name
     * ends in a "+", then any interface which begins with this name will match. If
     * this option is omitted, any interface name will match. @apiNote
     * --in-interface [!] name
     */

    public static final String iface = " -i "; // TO DO A function that will generate and view the available interfaces
                                             // for the system.

    /**
     * Name of an interface via which a packet is going to be sent (for packets
     * entering the FORWARD, OUTPUT and POSTROUTING chains). When the "!" argument
     * is used before the interface name, the sense is inverted. If the interface
     * name ends in a "+", then any interface which begins with this name will
     * match. If this option is omitted, any interface name will match. @apiNote
     * --out-interface [!] name
     */

    public static final String out = " -o ";

    /**
     * This means that the rule only refers to second and further fragments of
     * fragmented packets. Since there is no way to tell the source or destination
     * ports of such a packet (or ICMP type), such a packet will not match any rules
     * which specify them. When the "!" argument precedes the "-f" flag, the rule
     * will only match head fragments, or unfragmented packets. @apiNote -f,
     * --fragment
     */

    public static final String fragment = " -f ";

    /**
     * This enables the administrator to initialize the packet and byte counters of
     * a rule (during INSERT, APPEND, REPLACE operations). @apiNote --set-counters
     * PKTS BYTES
     */

    public static final String set = " -c ";

}
