package constants;

/** Constants that do not belong to any other category. @author Shrinidhi */
public class Others {

    /** Temporary file that will be overwritten. */
    public static final String tempSaveFile = "temp.iptables";
    /** File where backup is taken into at each run. */
    public static final String backUpFile = "backup.iptables";
    /**
     * Temporary file that will be created with the rules given by the user and
     * restored to iptables
     */
    public static final String newSaveFile = "save.iptables";
}