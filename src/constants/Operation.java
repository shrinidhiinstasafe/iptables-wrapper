package constants;

public class Operation {

    /**
     * Append one or more rules to the end of the selected chain. When the source
     * and/or destination names resolve to more than one address, a rule will be
     * added for each possible address combination.
     * 
     * @apiNote chain rule-specification
     */
    public static final String append = " -A ";

    /**
     * Delete one or more rules from the selected chain. There are two versions of
     * this command: the rule can be specified as a number in the chain (starting at
     * 1 for the first rule) or a rule to match.
     * 
     * @apiNote chain rule-specification
     * @apiNote chain rulenum
     */
    public static final String delete = " -D ";

    /**
     * Insert one or more rules in the selected chain as the given rule number. So,
     * if the rule number is 1, the rule or rules are inserted at the head of the
     * chain. This is also the default if no rule number is specified.
     * 
     * @apiNote chain [rulenum] rule-specification
     */
    public static final String insert = " -I ";

    /**
     * Replace a rule in the selected chain. If the source and/or destination names
     * resolve to multiple addresses, the command will fail. Rules are numbered
     * starting at 1.
     * 
     * @apiNote chain rulenum rule-specification
     */
    public static final String replace = " -R ";

    /**
     * List all rules in the selected chain. If no chain is selected, all chains are
     * listed. As every other iptables command, it applies to the specified table
     * (filter is the default), so NAT rules get listed by
     * <p>
     * 
     * <pre> iptables -t nat -n -L  </pre>
     * <p>
     * 
     * Please note that it is often used with the -n option, in order to avoid long
     * reverse DNS lookups. It is legal to specify the -Z (zero) option as well, in
     * which case the chain(s) will be atomically listed and zeroed. The exact
     * output is affected by the other arguments given. The exact rules are
     * suppressed until you use
     * <p>
     * 
     * <pre> iptables -L -v </pre>
     * <h2>asf</h2>
     * 
     * @apiNote [chain]
     */
    public static final String list = " -L ";

    /**
     * Flush the selected chain (all the chains in the table if none is given). This
     * is equivalent to deleting all the rules one by one.
     * 
     * @apiNote [chain]
     * 
     */
    public static final String flush = " -F ";

    /**
     * Zero the packet and byte counters in all chains. It is legal to specify the
     * option as well, to see the counters immediately before they are cleared. (See
     * above.)
     * 
     * @apiNote [chain]
     * 
     */
    public static final String zero = " -Z ";

    /**
     * Create a new user-defined chain by the given name. There must be no target of
     * that name already.
     * 
     * @apiNote chain
     * 
     */
    public static final String newChain = " -N ";

    /**
     * Delete the optional user-defined chain specified. There must be no references
     * to the chain. If there are, you must delete or replace the referring rules
     * before the chain can be deleted. The chain must be empty, i.e. not contain
     * any rules. If no argument is given, it will attempt to delete every
     * non-builtin chain in the table.
     * 
     * @apiNote [chain]
     * 
     */
    public static final String deleteChain = " -X ";

    /**
     * Set the policy for the chain to the given target. See the section TARGETS for
     * the legal <em>targets</em>. Only built-in (non-user-defined) chains can have
     * policies, and neither built-in nor user-defined chains can be policy targets.
     * 
     * @apiNote chain target
     * 
     */
    public static final String policy = " -P ";

    /**
     * Rename the user specified chain to the user supplied name. This is cosmetic,
     * and has no effect on the structure of the table.
     * 
     * @apiNote old-chain new-chain
     * 
     */
    public static final String renameChain = " -E ";
}
