package engine;

import entities.TablesEntity;

/**
 * This library is used to interpret the rules from a given iptables-save file.
 * This will read all rules and store it in our library. This will be useful for
 * understanding and displaying the rules in a user understandable manner.
 * 
 * @author Shrinidhi Rao shrinidhi@instasafe.com
 */

public class RulesInterpreter {

    private TablesEntity tablesEntity;

    private String completePage;

    public RulesInterpreter() {
    }

    public RulesInterpreter(TablesEntity tablesEntity) {
        this.tablesEntity = tablesEntity;
    }

    public TablesEntity getTablesEntity() {
        return this.tablesEntity;
    }

    public void setTablesEntity(TablesEntity tablesEntity) {
        this.tablesEntity = tablesEntity;
    }

    @Override
    public String toString() {
        return "{" + System.lineSeparator() + " tablesEntity='" + getTablesEntity() + "'" + "}";
    }

    public String getEncoded() {
        String temp = " ";

        // Filter Table
        if (tablesEntity.getFilterTable() != null) {
            temp += "*filter" + System.lineSeparator();
            temp += 
            if (tablesEntity.getFilterTable().getInput() != null) {
                
            }
        }

        // Raw Table

        // NAT Table

        // Mangle Table

        return temp;

    }

}