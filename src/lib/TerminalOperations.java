package lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import constants.Others;

/**
 * This is where all the calls to interact with the terminal will take place.
 * Currently only Ubuntu is supported.
 * 
 * @author Shrinidhi Rao shrinidhi@instasafe.com
 */
public class TerminalOperations {

    public List<String> getInterfaces() throws IOException {

        ProcessBuilder builder = new ProcessBuilder("/bin/sh", "-c", "ifconfig -s | awk '!/Iface/{print $1}'");
        builder.redirectErrorStream(true);
        // System.out.println(builder.command());
        Process process = builder.start();
        InputStream is = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String line = null;
        List<String> interfaces = new ArrayList<String>();
        while ((line = reader.readLine()) != null) {
            // System.out.println(line);
            interfaces.add(line);
        }
        return interfaces;

    }

    public void saveCurrentRules() throws IOException{
        saveCurrentRules(Others.tempSaveFile);
    }

    public void saveCurrentRules(String saveFile) throws IOException {
        ProcessBuilder builder = new ProcessBuilder("/bin/sh", "-c", "sudo iptables-save > "+saveFile);
        builder.redirectErrorStream(true);
        // System.out.println(builder.command());
        Process process = builder.start();
        InputStream is = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String line = null;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }


    }


}