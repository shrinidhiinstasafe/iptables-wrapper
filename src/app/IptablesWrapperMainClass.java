package app;

// Java code illustrating environment() method 
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import constants.Operation;
import constants.Others;
import constants.Table;
import constants.Target;
import entities.Rule;
import entities.TablesEntity;
import lib.FileOperations;
import lib.TerminalOperations;

public class IptablesWrapperMainClass {
	public static void main(String[] arg) throws IOException {
		// // creating the process
		// ProcessBuilder pb = new ProcessBuilder();

		// // map view of this process builder's environment
		// Map<String, String> envMap = pb.environment();

		// // checking map view of environment
		// for(Map.Entry<String, String> entry : envMap.entrySet())
		// {
		// // checking key and value separately
		// System.out.println("Key = " + entry.getKey() +
		// ", Value = " + entry.getValue());
		// }

		// RulesEncoder encoder = new RulesEncoder();
		// RuleBuilder builder = new RuleBuilder();

		// Environment url = Environment.valueOf("DEV");
		// System.out.println(url.getUrl());
		// System.out.println(Meow.Shrindhi.name().getClass());

		// System.out.println(Table.Filter.Chain.input.getChain());
		// System.out.println(Table.Nat.Chain.output.getChain());
		// System.out.println(Table.Filter.getTable());

		// System.out.println(Table.Filter.Chain.input.getChain());
		// System.out.println(Table.Mangle.getTable());

		// Parameters.gotoChain;

		Rule nasdf = new Rule.RuleBuilder().operation(Operation.append).target(Target.ACCEPT).build();

		System.out.println(nasdf.toString());

		TerminalOperations terminalOperations = new TerminalOperations();
		System.out.println("Print out all the interfaces supported by the system");

		List<String> interfaces = new ArrayList<String>();
		interfaces = terminalOperations.getInterfaces();

		Iterator iterator = interfaces.iterator();

		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}

		// terminalOperations.saveCurrentRules();

		FileOperations fileOperations = new FileOperations();
		fileOperations.takeBackUp();

		// HashMap<Integer, String> page = fileOperations.getLineNumbers(Others.tempSaveFile);

		// Iterator iterator2 = page.entrySet().iterator();

		// while (iterator2.hasNext()) {
		// 	System.out.println(iterator2.next());
		// }

		// int i = 0;
		// for(i=0; i<page.size();i++){
		// 	if(page.get(i).contains("*")){
		// 		System.out.println(page.get(i)+" found on line = "+i);
		// 	}
		// }
		// page.put(15, "Something new baby");

		// iterator2 = page.entrySet().iterator();

		// while (iterator2.hasNext()) {
		// 	System.out.println(iterator2.next());
		// }
		
		TablesEntity entity = fileOperations.generateTableEntity(fileOperations.convertFileToList());
		System.out.println(entity.getFilterTable().getInput().toString());
	} 
} 
